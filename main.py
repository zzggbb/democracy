# std imports
import os
import sys
import asyncio
import sqlite3

# 3rd party
import websockets
import flask


"""
network constants
"""

EXTERNAL_HOST = 'hifiboombox.ddns.net'
INTERNAL_HOST = "0.0.0.0"
HTTP_PORT = 8000
SOCKET_PORT = 9000


"""
database table definitions
"""

TABLES = {
    'accounts': """
                CREATE TABLE accounts (
                    id                TEXT    NOT NULL UNIQUE PRIMARY KEY,
                    username          TEXT    NOT NULL UNIQUE,
                    email_address     TEXT    NOT NULL UNIQUE,
                    password          TEXT    NOT NULL,
                    signup_time       TEXT    NOT NULL,
                    verified          BOOLEAN NOT NULL,
                    verification_id   TEXT    NOT NULL UNIQUE,
                    verification_time TEXT    NOT NULL
                )
                WITHOUT ROWID""",
}

"""
database and server constants
"""

SECRET_KEY_FILE_NAME = 'secret_key'
CONNECTION = sqlite3.connect('database.db')
CURSOR = connection.CURSOR()
APP = flask.Flask(__name__)


"""
database methods
"""

def setup_database():
    query = """
    SELECT name
    FROM sqlite_master
    WHERE type='table'
    """

    existing_tables = [row[0] for row in CURSOR.execute(query)]
    for table_name, table_schema in TABLES.items():
        if table_name not in existing_tables:
            CURSOR.execute(table_schema)

def account_registered(username, password):
    query = f"""
    SELECT username, password
    FROM accounts
    WHERE username='{username}' AND password='{password}'
    """

    results = CURSOR.execute(query)
    return len(results) == 1

def username_registered(username):
    # TODO: finish
    return False

def email_registered(email):
    # TODO: finish
    return False


"""
route handlers
"""

@APP.route('/')
def root():
    if 'username' in flask.session:
        return flask.redirect('/inside')
    else:
        return flask.render_template('outside.html', title='outside')

@APP.route('/login', methods=['GET', 'POST'])
def login():
    if flask.request.method == 'POST':
        # gather login form information
        username = flask.request.form['username']
        password = flask.request.form['password']

        if account_registered(username, password):
            session['username'] = username
            return flask.redirect('/inside')
        else:
            return flask.render_template('login.html', title='login', error=True)
    else:
        return flask.render_template('login.html', title='login', error=False)

@APP.route('/signup', methods=['GET', 'POST'])
def signup():
    errors = []
    email = None

    if flask.request.method == 'POST':
        # gather signup form information
        username = flask.request.form['username']
        email = flask.request.form['email']
        password = flask.request.form['password']

        # find errors in signup data
        if not username:
            errors.append('username is missing, fill it in')

        if not email:
            errors.append('email is missing, fill it in')

        if not password:
            errors.append('password is missing, fill it in')

        if username_registered(username):
            errors.append('username is already used by another user, use a different one')

        if email_registered(email):
            errors.append('email is already used by another user, use a different one')

        if not errors:
            # TODO: send the user an email with a link to /confirm?id=<UUID>
            pass

    return flask.render_template('signup.html', title='sign up', errors=errors, email=email)

@APP.route('/forgot')
def forgot():
    return flask.render_template('forgot.html', title='forgot')

@APP.errorhandler(404)
def page_not_found(e):
    return flask.render_template('page_not_found.html', title='page not found')

@APP.route('/inside')
def inside():
    if 'username' in flask.session:
        username = session['username']
        return f'you have entered the voting zone, {username}'
    else:
        return flask.redirect('/')

"""
main
"""

def main():
    setup_database()

    if not os.path.exists(SECRET_KEY_FILE_NAME):
        sys.exit(f'You are missing {SECRET_KEY_FILE_NAME}. Put some random crap in this file and try again.')

    with open('secret_key', 'r') as secret_key_file:
        APP.secret_key = secret_key_file.read()

    APP.run(INTERNAL_HOST, HTTP_PORT, debug=True)

if __name__ == '__main__':
    main()
