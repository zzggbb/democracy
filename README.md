## Installation

1. Create a virtual python environment

    This example creates an environment called `.env`

    ```bash
    $ python3 -m venv .env
    ```

2. Activate the python virtual environment

    Note, if you named your environment something other than `.env`, modifiy the following command.

    ```bash
    $ source .env/bin/activate
    ```

3. Install dependencies

    The dependencies are listed in a format parseable by `pip` in `requirements.txt`:

    ```bash
    $ pip3 install -r requirements.txt
    ```

4. Now you are ready to run the server.

## Running the server

1. Activate the python virtual environment, if you haven't already done so in the current shell

    ```bash
    $ source .env/bin/activate
    ```

2. Run the server

    ```
    $ python3 main.py
    ```

    You can kill the server by pressing `Control-C`

3. Deactivate the virtual env

    It's nice to know how to do it.

    ```
    $ deactivate
    ```
