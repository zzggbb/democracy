const SOCKET_PORT = 9000
const SOCKET_ADDRESS = `ws://${document.domain}:${SOCKET_PORT}`

function main() {

  var socket = new WebSocket(SOCKET_ADDRESS)

  /*
  A socket can emit 4 types of events:
  - open      When a connection with the socket is opened

  - close     When a connection with the socket is closed
    - code      unsigned short
    - reason    string
    - wasClean  boolean

  - message   When data is sent to this context
    - data         string?? data of the message
    - origin       string??
    - lastEventId  string?? unique id for the message
    - ports        array[MessagePort]

  - error     When a connection with the socket is closed because of an error

  A socket has 2 methods:
  - close([code[, reason])    Close the connection
  - send(data)                Transmit data
  */

  socket.addEventListener('open', (event) => {
    console.log(`OPEN EVENT`)
  })

  socket.addEventListener('close', (event) => {
    console.log(`CLOSE EVENT: code:${event.code} reason:${event.reason} wasClean:${event.wasClean}`)
  })

  socket.addEventListener('message', (event) => {
    console.log(`MESSAGE EVENT: data: ${event.data}`)
  })

  socket.addEventListener('error', (event) => {
    console.log(`ERROR EVENT`)
  })
}
